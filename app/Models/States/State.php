<?php

namespace App\Models\States;

use Illuminate\Database\Eloquent\Model;
use App\Models\Countries\Country;
use App\Models\Cities\City;

class State extends Model
{
    
    protected $table = 'states';
    protected $fillable = ['statename','stateabbr','country_id'];

    public  function country()
    {
        return $this->belongsTo('App\Models\Countries\Country');
    }

    public function cities()
    {
        return $this->hasMany('App\Models\Cities\City');
    }


}
