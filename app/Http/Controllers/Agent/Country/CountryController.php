<?php

namespace App\Http\Controllers\Agent\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries\Country;
use Session;
use Auth;
use App\Models\States\State;

class CountryController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }
    public function index()
    {
    	$country = Country::all();

    	return view('agent.country.create',compact('country'));

    }

    public function create(Request $request)
    {
    	$country = Country::all();
    	//dd($country);

    	return view('agent.country.create',compact('country'));
    }

    public function store(Request $request)
    {

    	    $validatedData = $request->validate([

            'countryname' => 'required|string|max:255',
            'countrysh' => 'required|string',
        ]);

        $country = Country::create($validatedData);
        //dd($country)->toArray();
        	
        return redirect()->back()->with('success', 'Country successfully saved');		
        	}
        	public function stateindex(Request $request)
        	{
        		$states = State::all();
        			//dd($states)->toArray();
        		return view('agent.country.create',compact('states'));
        	}
        	public function statestore(Request $request)
				{
				
				$statevalidate = $request->validate([
        		'statename' => 'required|string|max:255',
        		'stateabbr' => 'required|string|max:255',
                'country_id' => 'sometimes',

        		]);

        		$states = State::create($statevalidate);
        		//dd($state);

        		return redirect()->back()->with('success','State successfully saved');
				}   

				public function deletestate(Request $request, $id)
				{
					$states = State::where('statename','>' ,'0')->delete();
					return view('agent.country.create',compact('states'))->with('error','states deleted');
				}
     


     public function edit($id)
     {

     	$country = Country::findorFail($id);
     	//dd($country_edit);
     	return view('agent.country.create',compact('country'));
     }
    public function update(Request $request, $id)
    {
    	  $request->validate(
                [
                'countryname' => 'required|max:255',
                 ]);

        $coun =
            [
                'countryname' => $request->countryname,
            ];

        $country=Country::whereId($id)->update($coun);
        //dd($country_update);
        return view('agent.country.create',compact('country'))->with('update', 'Country successfully updated!');

    }
      public function stateupdate(Request $request, $id)
    {
    	  $request->validate(
                [
                'statename' => 'required|max:255',
                'stateabbr' => 'required|max:255',
                'country_id' => 'sometimes',
                 ]);

        $st =
            [
                'statename' => $request->statename,
                'stateabbr' => $request->stateabbr,
                'country_id' => $request->country_id,
            ];

        $states=State::whereId($id)->update($st);
        //dd($country_update);
        return view('agent.country.create',compact('states'))->with('update', 'Country successfully updated!');

    }
     public function destroy(Request $request, $id)
    	{

            $country = Country::findorFail($id)->delete();
             // dd($country)->toArray();
            return redirect()->back()
                   ->with('error','country deleted successfully');
    }

    

    }

