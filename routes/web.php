<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Admin;


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

	Route::get('home', 'HomeController@index')->name('home');
	Route::get('admin/routes', 'HomeController@admin')->middleware('admin');

	Route::get('dashboard','Admin\DashboardController@index')->name('dashboard');
     

Route::get('offices','Agent\OfficeController\OfficeController@index')->name('office.index');
Route::get('office/create','Agent\OfficeController\OfficeController@create')->name('office.create');
Route::post('office/store','Agent\OfficeController\OfficeController@store')->name('office.store');
Route::get('office/edit/{id}','Agent\OfficeController\OfficeController@edit')->name('office.edit');
Route::post('office/create/{id}','Agent\OfficeController\OfficeController@update')->name('office.update');
Route::delete('offices/create/{id}','Agent\OfficeController\OfficeController@destroy')->name('office.destroy');





Route::get('countries','Agent\Country\CountryController@index')->name('country.index');
Route::get('countries/create','Agent\Country\CountryController@create')->name('country.create');
Route::post('countries/','Agent\Country\CountryController@store')->name('country.save');
Route::get('countries/edit/{id}','Agent\Country\CountryController@edit')->name('country.edit');
Route::post('countries/create/{id}','Agent\Country\CountryController@update')
 ->name('country.update');
Route::delete('countries/create/{id}','Agent\Country\CountryController@destroy')
->name('country.destroy');


Route::get('countries/create','Agent\Country\CountryController@stateindex')->name('state.index');
Route::post('countries/store','Agent\Country\CountryController@statestore')->name('state.store');
Route::post('countries/create/{id}','Agent\Country\CountryController@stateupdate')
 ->name('state.update');
Route::delete('countries/states/{id}','Agent\Country\CountryController@deletestate')->name('state.destroy');



Route::get('users/index','Admin\User\UserController@index')->name('user.index'); 
Route::get('users/create/{id}','Admin\User\UserController@create')->name('user.create'); 
Route::post('users/store','Admin\User\UserController@store')->name('users.store');
      
   


Route::get('company/info','admin\CompanyController@index')->name('company.index');
Route::post('company/create','admin\CompanyController@create')->name('company.create');


 
Route::get('rule/tiers','Rules\RulesController\RulesController@index')->name('rule.tiers');
Route::post('ajax/{id}','Rules\RulesController\RulesController@post');

Route::post('managetier/{id}','Rules\RulesController\RulesController@managetier');
Route::post('ruledelete/{idelete}','Rules\RulesController\RulesController@delete');
Route::post('tierdelete/{id}','Rules\RulesController\RulesController@tierdelete');
Route::post('percent/{id}','Rules\RulesController\RulesController@managepercent');

